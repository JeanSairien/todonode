# hello, Bienvenue sur le projet TodoListApi

----
# qu'est ce que TodoList?
see [www.codementor.io](https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd)

> Le projet TodoList provient du tutoriel qui suit le lien ci-dessus, il consiste a mettre en place  une api basée sur node et express ainsi qu'une base d'api Rest permettant la gestion de tache via une page web [localhost](http://localhost:3000/tasks)

----

# Les Pré-requis

**System installation**

	* nodejs

	* mongodb

	* insomnia

**Npm installation**

	* nodemon

	* express

	* mongoose 
	
----

# Les Commits

----
les commits doivent autant que possible respecter la normalisation suivante :

1. [ADD] pour tout ajout de nouveaux codes 
2. [TRY] pour tous nouveaux codes en vue de tests
3. [FIX] pour toutes corrections de codes
4. [DEL] pour toutes suppressions de codes
5. [MOVE] pour tout déplacements de codes 

----

# Objectifs

### Cette application sert de test pour de la gestion de taches via une api rest

### Ce tutoriel a été suivit dans l'optique de pratiquer node reposant sur une api rest

----

# Fonctionnalités

**les fonctionnalités de l'api sont accecibles via des paramettres passer dans l'url ainsi qu'au bon parametrages du header de connection**

**Ex:** 
_http://localhost:3000/tasks/5ca4148c28e45a204ed36158_

* **POST** permet de créer une taches 

	_localhost:3000/tasks/_

* **GET** permet la récupération des taches existantes

 	il est possible de cibler une taches avec son identifiant
 	
	_localhost:3000/tasks/taskId_ 

* **UPDATE** permet la mise a jour d'une tache en spécifiants son identifiant 	

	_localhost:3000/tasks/taskId_

* **DELETE** permet la suppression d'une tache en spécifiants son identifiant
	
	_localhost:3000/tasks/taskId_

----

# Prévisions

### Création d'un crud fonctionnel

### Amélioration et customisation
	
### Mise en ligne sur un mode jamstack

### Mise en place d'une librairie front

----
### Arborescence
```
todoListApi                            
├── api 				Contient les composants de l'api rest
│   ├── controllers
│   │	│
│	│   └──todoListController.js    Le fichier qui gere le crud      
│   ├── models     
│	│	│
│	│   └──todoListModel.js         Le fichier qui contient le model  
│   └── routes   			
│		│
│	    └──todoListRoutes.js    	La gestion des routes  
├── node_modules    			Les dossiers des dépendances installé
├── package-lock.json    		Le fichier qui contient les sources des dépendances de maniere figé
├── package.json    			Le fichier d'installation des dépendances	
└── readme.md    			Documentation du projet      
```

----

# Dépendances

**Les dépendances ont été mise a jours car celle indiquées dans le tutoriel sont déprécier**

	body-parser: 1.18.3
    express: 4.16.4
    mongoose: 5.4.21

----
# Test

Les tests de requetage sont effectuer via le client linux 

**insomnia.react**


----

# Remarque

*Bien évidement, le code a été repris du tutoriel et demande a etre modifier, l'objectif premier etant d'avoir une base propre et modifiable pour s'inspiré, l'indulgence est donc de mise*


----



